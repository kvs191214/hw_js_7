console.log('1. Перевірити, чи є рядок паліндромом')

function isPalindrome(str) {

    str = str.toLowerCase();
    let len = str.length;
    for (let i = 0; i < len; i++) {
        if (str[i] !== str[len - 1 - i]) {
            return false;
        }
    }
    return true;
}

console.log(isPalindrome('Madam'));
console.log(isPalindrome('Palindrome'))

console.log('2. Створіть функцію, яка перевіряє довжину рядка.');

function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}

console.log(checkStringLength('checked string', 20));
console.log(checkStringLength('checked string', 10));

console.log('3. Створіть функцію, яка визначає скільки повних років користувачу.');

function howOld(birthdayDate) {
    let yearsFull = ((today - birthdayDate) / 1000 / 60 / 60 / 24 / 365).toString();
    let commaIndex = yearsFull.indexOf('.');
    return +(yearsFull.slice(0, commaIndex))
}

const today = Date.now();
let enterYourBirthday = new Date(
    prompt('Введіть вашу дату народження у форматі рррр-мм-дд , або рік народження')
);

while (isNaN(howOld(enterYourBirthday)) || howOld(enterYourBirthday) < 0) {
    enterYourBirthday = new Date(prompt(
        'Невірно введена дата! Введіть вашу дату народження у форматі рррр-мм-дд , або рік народження'
    ));
}

alert(howOld(enterYourBirthday));
